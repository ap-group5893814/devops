FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
WORKDIR /app
EXPOSE 8089

FROM mcr.microsoft.com/dotnet/sdk:8.0 as build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["DeliveryApp.Api/DeliveryApp.Api.csproj", "DeliveryApp.Api/"]
RUN dotnet restore "DeliveryApp.Api/DeliveryApp.Api.csproj"
COPY . . 
WORKDIR "/src/DeliveryApp.Api"
RUN dotnet build "DeliveryApp.Api.csproj" -c $BUILD_CONFIGURATION -o /app/build 

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "DeliveryApp.Api.csproj" -c $BUILD_CONFIGURATION -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "DeliveryApp.Api.dll"]

