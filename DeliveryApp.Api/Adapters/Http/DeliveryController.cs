using System.ComponentModel.DataAnnotations;
using Api.Controllers;
using Microsoft.AspNetCore.Mvc;
using DeliveryApp.Api.Logic;

namespace DeliveryApp.Api.Adapters.Http;

public class DeliveryController : DefaultApiController
{     
    public override async Task<IActionResult> Health()
    {
        return Ok("Healthy");   
    }
    
    public override async Task<IActionResult> GetSum([FromRoute(Name = "number1"), Required] long number1, [FromRoute(Name = "number2"), Required] long number2)
    {
        var sum = number1 + number2;
        return Ok(sum);
    }
}