﻿namespace DeliveryApp.Api
{
    public class Settings
    {
        public int PORT {get; set; }
        public string CONNECTION_STRING { get; set; }
    }
}