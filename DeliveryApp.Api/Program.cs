using DeliveryApp.Api;

namespace DeliveryApp.ApiDeliveryApp.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var port = Environment.GetEnvironmentVariable("PORT");          
            Console.Write($"I'm working on port: {port}");

            if (string.IsNullOrEmpty(port)) port = 8080.ToString();
            
            var host = CreateHostBuilder(args, port).Build();
            using (var scope = host.Services.CreateScope())
            {               
            }            
            
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args, string port)
            => Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(
                    webBuilder => 
                    {
                        webBuilder.UseStartup<Startup>();
                        webBuilder.UseUrls($"http://*:{port}"); // Установка порта прослушивания
                    });
    }
}